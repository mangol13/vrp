package com.vet.safrov.utils;

/**
 * international system
 */
public final class SI {

    private static int METERS_IN_KM = 1_000;
    private static double SPEED_FACTOR = 3.6;

    public static double kmToM(double km) {
        return km * METERS_IN_KM;
    }

    public static double metersPerSecond(double kmPerH) {
        return kmPerH / SPEED_FACTOR;
    }

    public static double kmPerHour(double mPerS) {
        return mPerS * SPEED_FACTOR;
    }

}
