package com.vet.safrov.utils;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class TimeUtils {

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("H:mm");
    private static final DateTimeFormatter TIME_FORMATTER_WITH_SECONDS = DateTimeFormatter.ofPattern("H:mm:ss");
    public static final int SECONDS_IN_DAY = 24 * 60 * 60;


    public static Function<String, LocalTime> stringToLocalTime = str ->
        LocalTime.parse(str, TIME_FORMATTER);


    public static Function<LocalTime, String> localTimeToString = lt ->
        lt.format(TIME_FORMATTER);

    public static Function<LocalTime, Integer> localTimeToDaySeconds = lt ->
        lt.toSecondOfDay();

    public static Function<Integer, LocalTime> daySecondsToLocalTime = seconds ->
        LocalTime.ofSecondOfDay(seconds);

    public static Function<String, Integer> stringToDaySeconds = stringToLocalTime.andThen(localTimeToDaySeconds);

    public static Function<Integer, String> daySecondsTostring = daySecondsToLocalTime.andThen(localTimeToString);

    public static Function<Integer, String> daySecondsToHmmss = daySecondsToLocalTime.andThen(lt -> lt.format(TIME_FORMATTER_WITH_SECONDS));
}
