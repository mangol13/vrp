package com.vet.safrov.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class JsonUtils {

    public static Map<String, ?> deserialize(URL url) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, ?> map = mapper.readValue(url, Map.class);
        return map;
    }
}
