package com.vet.safrov.exception;

public class VPRSException extends RuntimeException {

    public VPRSException(String message) {
        super(message);
    }

    public VPRSException(String message, Throwable th) {
        super(message, th);
    }

}
