package com.vet.safrov.service;

import com.vet.safrov.model.TimePeriod;
import com.vet.safrov.model.VRPData;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.vet.safrov.utils.SI.kmPerHour;
import static com.vet.safrov.utils.TimeUtils.daySecondsTostring;
import static com.vet.safrov.utils.TimeUtils.stringToDaySeconds;

public class VrpSolutionImpl implements VrpSolution {

    private final VRPData vrpData;

    public VrpSolutionImpl(VRPData vrpData) {
        this.vrpData = vrpData;
    }

    BiPredicate<Integer, Integer> isValidLocation = (location, nrOfLocations) ->
            location > 0 && location <= nrOfLocations;

    @Override
    public int calculate(int locX, int locY, String t0AsStr) {

        if (isValidLocation.negate().test(locX, vrpData.getNrOfLocations())  || isValidLocation.negate().test(locY, vrpData.getNrOfLocations())) {
            throw new IllegalArgumentException(String.format("Invalid location. Location must be between 1 and %d", vrpData.getNrOfLocations()));
        }

        // convert to 0 indexed
        int location1 = locX - 1;
        int location2 = locY - 1;

        if (location1 == location2) {
            return 0;
        }

        int t0 = stringToDaySeconds.apply(t0AsStr);
        List<TimePeriod> preparedTimePeriods = prepareTimePeriods(t0);


        int t = t0;
        double distance = vrpData.getDistance(location1, location2);
        int t1 = 0;

        for (TimePeriod tp : preparedTimePeriods) {
            double speed = vrpData.getSpeedVariation(tp, location1, location2);

            t1 =  t + new Double(distance / speed).intValue();

            distance -= speed * (tp.getEnd() - t);
            t = tp.getEnd();

            System.out.println(String.format("TimePeriod=%s; v=%.1f; t=%d; t1=%d; d=%.1f",
                    tp, kmPerHour(speed), t, t1, distance));

            if (t1 <= tp.getEnd()) {
                break;
            }
        }

        return t1 - t0;
    }

    private List<TimePeriod> prepareTimePeriods(int t0) {
        int t0Idx = IntStream.range(0, vrpData.getTimePeriods().size())
                .filter(idx -> vrpData.getTimePeriods().get(idx).getEnd() > t0)
                .findFirst()
                .getAsInt();

        if (t0Idx == 0) {
            return vrpData.getTimePeriods();
        }

        List<TimePeriod> result = new ArrayList<>(vrpData.getTimePeriods().subList(t0Idx, vrpData.getTimePeriods().size() - 1));
        vrpData.getTimePeriods().subList(0, t0Idx).stream()
                .map(TimePeriod::nextDay)
                .collect(Collectors.toCollection(() -> result));

        return result;
    }

}
