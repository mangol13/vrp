package com.vet.safrov.service;

public interface VrpSolution {

    /**
     *
     * @param location1 index of location1 (first is 1) from distanceMatrix and speedVariationWithTimeSlices matrixes
     * @param location2 index of location2 (first is 1) from distanceMatrix and speedVariationWithTimeSlices matrixes
     * @param t0 start time in <b>h:mm<b/> format
     * @return time spent in seconds for getting from location1 to location2 starting at t0
     */
    int calculate(int location1, int location2, String t0);
}
