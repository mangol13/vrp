package com.vet.safrov.model;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.vet.safrov.utils.TimeUtils.SECONDS_IN_DAY;
import static com.vet.safrov.utils.TimeUtils.daySecondsTostring;
import static com.vet.safrov.utils.TimeUtils.stringToDaySeconds;

public class TimePeriod {
    private final String name;
    // seconds of the day
    private final int start;
    // seconds of the day
    private final int end;


    public TimePeriod(Map.Entry<String, List<String>> entry) {
        this(entry.getKey(), entry.getValue());
    }

    public TimePeriod(String name, List<String> periods) {
        this(name, stringToDaySeconds.apply(periods.get(0)), stringToDaySeconds.apply(periods.get(1)));
    }

    private TimePeriod(String name, int start, int end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public String getStartAsString() {
        return daySecondsTostring.apply(start % SECONDS_IN_DAY);
    }

    public String getEndAsString() {
        return daySecondsTostring.apply(end % SECONDS_IN_DAY);
    }

    public TimePeriod nextDay() {
        return new TimePeriod(this.name, this.start + SECONDS_IN_DAY, this.end + SECONDS_IN_DAY);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimePeriod that = (TimePeriod) o;
        return start == that.start &&
                end == that.end &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, start, end);
    }

    @Override
    public String toString() {
        return "TimePeriod{" +
                "name='" + name + '\'' +
                ", start=" + getStartAsString() +
                ", end=" + getEndAsString() +
                '}';
    }

}
