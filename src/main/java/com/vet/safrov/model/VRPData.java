package com.vet.safrov.model;

import com.vet.safrov.exception.VPRSException;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.vet.safrov.utils.SI.kmToM;
import static com.vet.safrov.utils.SI.metersPerSecond;

public class VRPData {

    private List<TimePeriod> timePeriods;
    private Map<String, Double> speedCategories;
    private List<List<Double>> distanceMatrix;
    private Map<String, List<List<String>>> speedVariationWithTimeSlices;

    private VRPData() { }


    public double getDistance(int location1, int location2) {
        // making D[i][j] symmetric and getting from upper right matrix
        int l1 = Math.min(location1, location2);
        int l2 = Math.max(location1, location2);
        return kmToM(distanceMatrix.get(l1).get(l2));
    }

    public List<TimePeriod> getTimePeriods() {
        return timePeriods;
    }

    public double getSpeedVariation(TimePeriod timePeriod, int location1, int location2) {
        String speedCategory = speedVariationWithTimeSlices.get(timePeriod.getName()).get(location1).get(location2);
        return speedCategories.get(speedCategory);
    }

    public int getNrOfLocations() {
        return distanceMatrix.size();
    }


    public static class Builder {
        static final String DISTANCE_MATRIX = "distanceMatrix";
        static final String TIME_PERIODS = "timePeriods";
        static final String SPEED_CATEGORIES = "speedCategories";
        static final String SPEED_VARIATION_WITH_TIME_SLICES = "speedVariationWithTimeSlices";

        private final Map<String, ?> json;
        private final VRPData vrpData = new VRPData();

        public Builder(Map<String, ?> json) {
            this.json = json;
        }

        public VRPData build() {
            validate();
            return this.vrpData;
        }

        public Builder buildTimePeriods() {
            Map<String, List<String>> timePeriodsJson = (Map<String, List<String>>) json.get(TIME_PERIODS);

            vrpData.timePeriods = Collections.unmodifiableList(timePeriodsJson.entrySet().stream()
                    .map(TimePeriod::new)
                    .sorted(Comparator.comparingInt(TimePeriod::getStart))
                    .collect(Collectors.toList()));

            return this;
        }

        public Builder buildSpeedCategories() {
            Map<String, Double> speedCategoriesJson = (Map<String, Double>) json.get(SPEED_CATEGORIES);

            vrpData.speedCategories = Collections.unmodifiableMap(speedCategoriesJson.entrySet().stream()
                    .collect(Collectors.toMap(
                            (Map.Entry<String, Double> entry) -> entry.getKey(),
                            (Map.Entry<String, Double> entry) -> metersPerSecond(entry.getValue()))
                    ));

            return this;
        }

        public Builder buildDistanceMatrix() {
            List<List<Double>> distanceMatrixJson = (List<List<Double>>) json.get(DISTANCE_MATRIX);
            vrpData.distanceMatrix = Collections.unmodifiableList(distanceMatrixJson);

            return this;
        }

        public Builder buildSpeedVariationWithTimeSlices() {
            Map<String, List<List<String>>> speedVariationWithTimeSlicesJson = (Map<String, List<List<String>>>) json.get(SPEED_VARIATION_WITH_TIME_SLICES);
            vrpData.speedVariationWithTimeSlices = new LinkedHashMap<>(speedVariationWithTimeSlicesJson);

            return this;
        }

        private void validate() {
            validateDistanceMatrix();
            validateSpeedVariationWithTimeSlicesMatrix();
        }

        private void throwMatrixInvalidDimensionException(String property) {
            throw new VPRSException(String.format("%s must be %d X %d matrix", property, vrpData.getNrOfLocations(), vrpData.getNrOfLocations()));
        }

        private Predicate<List<?>> invalidMatrixPredicate = l -> l.size() != vrpData.getNrOfLocations();

        private void validateDistanceMatrix() {
            vrpData.distanceMatrix.stream()
                    .filter(invalidMatrixPredicate)
                    .findAny()
                    .ifPresent(l -> throwMatrixInvalidDimensionException(DISTANCE_MATRIX));
        }

        private void validateSpeedVariationWithTimeSlicesMatrix() {
            vrpData.speedVariationWithTimeSlices.values().stream()
                    .forEach(l -> {
                        if (invalidMatrixPredicate.test(l)) {
                            throwMatrixInvalidDimensionException(SPEED_VARIATION_WITH_TIME_SLICES);
                        }

                        l.stream()
                                .filter(invalidMatrixPredicate)
                                .findAny()
                                .ifPresent(l2 -> throwMatrixInvalidDimensionException(SPEED_VARIATION_WITH_TIME_SLICES));
                    });

        }

    }

}
