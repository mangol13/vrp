package com.vet.safrov;

import static com.vet.safrov.utils.TimeUtils.daySecondsToHmmss;
import static org.junit.Assert.assertEquals;

import com.vet.safrov.exception.VPRSException;
import com.vet.safrov.model.VRPData;
import com.vet.safrov.service.VrpSolution;
import com.vet.safrov.service.VrpSolutionImpl;
import com.vet.safrov.utils.JsonUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.function.Function;

/**
 * Unit tests for VRP solution.
 */
public class AppTest {

    Function<String, Map<String, ?>> getMockData = path -> {
        try {
            return JsonUtils.deserialize(AppTest.class.getResource(path));
        } catch (IOException e) {
            throw new VPRSException(String.format("Invalid json format while parsing %s", path), e);
        }
    };

    Function<Map<String, ?>, VRPData> createVrpData = map ->
            new VRPData.Builder(map)
                    .buildTimePeriods()
                    .buildSpeedCategories()
                    .buildDistanceMatrix()
                    .buildSpeedVariationWithTimeSlices()
                    .build();

    Function<String, VRPData> createVrpDataFromMock = getMockData.andThen(createVrpData);

    @Test
    public void scenario1With_2_3_7_50() {
        VRPData vrpData = createVrpDataFromMock.apply("/scenario1.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        int timeSpentInSeconds = vrpSolution.calculate(2, 3, "7:50");
        String result = daySecondsToHmmss.apply(timeSpentInSeconds);
        System.out.println(String.format("Result: %s", result));

        assertEquals(result, "2:58:00");
    }

    @Test
    public void scenario1With_2_3_19_15() {
        VRPData vrpData = createVrpDataFromMock.apply("/scenario1.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        int timeSpentInSeconds = vrpSolution.calculate(2, 3, "19:15");
        String result = daySecondsToHmmss.apply(timeSpentInSeconds);
        System.out.println(String.format("Result: %s", result));

        assertEquals(result, "1:16:30");
    }

    @Test
    public void scenario1With_3_1_8_45() {
        VRPData vrpData = createVrpDataFromMock.apply("/scenario1.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        int timeSpentInSeconds = vrpSolution.calculate(3, 1, "8:45");
        String result = daySecondsToHmmss.apply(timeSpentInSeconds);
        System.out.println(String.format("Result: %s", result));

        assertEquals(result, "1:21:00");
    }

    @Test
    public void scenario1With_2_3_23_50() {
        VRPData vrpData = createVrpDataFromMock.apply("/scenario1.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        int timeSpentInSeconds = vrpSolution.calculate(2, 3, "23:50");
        String result = daySecondsToHmmss.apply(timeSpentInSeconds);
        System.out.println(String.format("Result: %s", result));

        assertEquals(result, "0:54:00");
    }



    @Test
    public void sameLocationsShouldReturnZero() {
        VRPData vrpData = createVrpDataFromMock.apply("/scenario1.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        int timeSpentInSeconds = vrpSolution.calculate(2, 2, "7:50");

        assertEquals(timeSpentInSeconds, 0);
    }

    @Test(expected = VPRSException.class)
    public void invalidDistanceMatrixShouldTrowException() {
        VRPData vrpData = createVrpDataFromMock.apply("/invalid_distance_matrix.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        vrpSolution.calculate(1, 2, "7:00");
    }

    @Test(expected = VPRSException.class)
    public void invalidSpeedVariationMatrixShouldTrowException() {
        VRPData vrpData = createVrpDataFromMock.apply("/invalid_speedVariation_matrix.json");
        VrpSolution vrpSolution = new VrpSolutionImpl(vrpData);
        vrpSolution.calculate(1, 2, "7:00");
    }

}
