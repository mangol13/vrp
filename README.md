# VRP Solution #

This README would normally document whatever steps are necessary to get your application up and running.

### Setup ###

* Java 8 and higher
* Maven 3.5 and higher

### How to run unit tests with mock ###

* In **src/test/resources** you can find mock files in json format. You can change or add another mocks
* You can run Unit Tests from src/test/java/com/vet/safrov/AppTest.java where you can find some tests. You can add your own.
* The implementation of VRP method is at src/main/java/com/vet/safrov/service/VrpSolutionImpl#calculate(int X, int Y, String t0).
<br/>
X, Y - integers between 1 and N in matrix N*N size. t0 - is time as String in "h:mm" format.



### **Mock structure** ###  
  
#### distanceMatrix   

    [  
        [ 0, 20.0, 30.0 ],  
        [ 0,    0, 90.0 ],  
        [ 0,    0,    0 ]  
    ]

distanceMatrix is distances between locations (i, j) and should be symmetric d[i][j] == d[j][i], so just provide the upper right side, d[i][j] where i < j. The rest keep as 0 as they are ignored.

#### timePeriods

    {  
      "T1": ["0:00", "7:00"],  
      "T2": ["7:00", "8:00"],  
      "T3": ["8:00", "10:00"],  
      "T4": ["10:00", "12:00"],  
      "T5": ["12:00", "14:00"],  
      "T6": ["14:00", "17:00"],  
      "T7": ["17:00", "20:00"],  
      "T8": ["20:00", "23:00"],  
      "T9": ["23:00", "23:59"]  
    }

These are Time slices T1, T2, T3, ... TK

#### speedCategories

    {  
      "C0": 0.0,  
      "C1": 10.0,  
      "C2": 20.0,  
      "C3": 30.0,  
      "C4": 40.0,  
      "C5": 50.0,  
      "C6": 60.0,  
      "C7": 70.0,  
      "C8": 80.0,  
      "C9": 90.0,  
      "C10": 100.0  
    }

These are Speed Categories C1, C2, C3, ... CR in km/h


#### speedVariationWithTimeSlices

    {  
      "T1": [  
        [  "C0", "C10", "C10" ],  
      [ "C10", "C0", "C10" ],  
      [ "C10", "C10", "C0" ]  
      ],  
      "T2": [  
        [  "C0", "C4", "C5" ],  
      [  "C6", "C0", "C6" ],  
      [  "C5", "C4", "C0" ]  
      ],  
      "T3": [  
        [  "C0", "C2", "C3" ],  
      [  "C2", "C0", "C2" ],  
      [  "C2", "C3", "C3" ]  
      ],  
      "T4": [  
        [  "C0", "C5", "C5" ],  
      [  "C5", "C0", "C5" ],  
      [  "C5", "C4", "C5" ]  
      ],  
      "T5": [  
        [  "C0", "C2", "C3" ],  
      [  "C2", "C0", "C2" ],  
      [  "C2", "C3", "C3" ]  
      ],  
      "T6": [  
        [  "C0", "C5", "C6" ],  
      [  "C5", "C0", "C5" ],  
      [  "C5", "C4", "C5" ]  
      ],  
      "T7": [  
        [  "C0", "C6", "C6" ],  
      [  "C5", "C0", "C5" ],  
      [  "C5", "C4", "C5" ]  
      ],  
      "T8": [  
        [  "C0", "C10", "C10" ],  
      [ "C10", "C0", "C10" ],  
      [ "C10", "C10", "C0" ]  
      ],  
      "T9": [  
        [  "C0", "C10", "C10" ],  
      [ "C10", "C0", "C10" ],  
      [ "C10", "C10", "C0" ]  
      ]  
    }

Each time slice has it's own speed category matrix between locations. The matrix could be asymmetric. Tk[i][j] represents the speed category between locations(i,j) in time period k.
> **Note:** Tk matrix must have same size as distance matrix.